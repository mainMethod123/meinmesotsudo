/**
* This sample program shows a PID controller for line following.
*
* Robot construction: Educator Vehicle
*
* References:
* http://robotsquare.com/wp-content/uploads/2013/10/45544_educator.pdf
*http://thetechnicgear.com/2014/03/howto-create-line-following-robot-using-mindstorms/
*/

#include "ev3api.h"
#include "app.h"

#if defined(BUILD_MODULE)
#include "module_cfg.h"
#else
#include "kernel_cfg.h"
#endif

#define DEBUG

#ifdef DEBUG
#define _debug(x) (x)
#else
#define _debug(x)
#endif

/**
* Define the connection ports of the sensors and motors.
* By default, this application uses the following ports:
* Touch sensor: Port 2
* Color sensor: Port 3
* Left motor:   Port B
* Right motor:  Port C
*/
const int touch_sensor = EV3_PORT_2, color_sensor = EV3_PORT_3, left_motor = EV3_PORT_B, right_motor = EV3_PORT_C, gyro_sensor = EV3_PORT_4;

static void button_clicked_handler(intptr_t button) {
    switch(button) {
    case BACK_BUTTON:
#if !defined(BUILD_MODULE)
        syslog(LOG_NOTICE, "Back button clicked.");
#endif
        break;
    }
}

void main_task(intptr_t unused) {
    // Register button handlers
    ev3_button_set_on_clicked(BACK_BUTTON, button_clicked_handler, BACK_BUTTON);

    // Configure motors
    ev3_motor_config(left_motor, LARGE_MOTOR);
    ev3_motor_config(right_motor, LARGE_MOTOR);

    // Configure sensors
    ev3_sensor_config(touch_sensor, TOUCH_SENSOR);
    ev3_sensor_config(color_sensor, COLOR_SENSOR);
    ev3_sensor_config(gyro_sensor, GYRO_SENSOR);

/*-----------------------------------------------------------*/
/*    ワークエリア                                           */
/*-----------------------------------------------------------*/
//画面表示のカウント
int n = 1;
//基準との差分
int error = 0;
//微分ゲイン
int diff = 0;
//ライントレースの基準値
float midpoint = 0;
//ライントレースの光量
int get_color = 0;
//ライントレースの光量（補正）
int color_now = 0;
//（ひとつ前の）基準との差分
int lasterror = 0;
    //steer
    int steer = 0;

/*-----------------------------------------------------------*/
/*    定数エリア                                             */
/*-----------------------------------------------------------*/
//白
int white = 45;
//黒
int black = 2;
//Kp
float kp = 1.2;
//Kd
float kd = 100.0;
//モータパワー
int power = -60;
//Wait
int wait = 1;

/*-----------------------------------------------------------*/
/*    開始処理                                               */
/*-----------------------------------------------------------*/
/*---- <ライントレースの基準を取得> ----*/
//若干白よりを走る
midpoint = ((white - black) / 2 + black) * 1.175; 

/*-----------------------------------------------------------*/
/*    主処理                                                 */
/*-----------------------------------------------------------*/

    //ボタンを押すと止まる
    while (!(ev3_button_is_pressed(ENTER_BUTTON))) {
//現在の光量抽出
        get_color = ev3_color_sensor_get_reflect(color_sensor);

//基準との差分計算
error = midpoint - color_now;

        //微分ゲイン計算
        diff = (error - lasterror) / wait;

//Steer値を計算
        steer = kp * error + kd * diff;

//ev3_motor_steer関数へ
        ev3_motor_steer(left_motor, right_motor, power, steer);

//基準との差分を退避
        lasterror = error;

        //次の命令までに待つ時間（ミリ秒）
        tslp_tsk(wait);
    }

    //回転ストップ	
    ev3_motor_stop(left_motor,true);
    ev3_motor_stop(right_motor,true);
}

